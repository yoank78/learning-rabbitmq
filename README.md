# Learning Rabbitmq

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/yoank78/learning-rabbitmq)

# Spring Boot demo application to test RabbitMQ

Voir les vidéos Youtube:
- https://www.youtube.com/watch?v=yDpJiJmf1wo&list=PLn6POgpklwWqiqKEriklbvbSd60-weOqh&index=2
- https://www.youtube.com/watch?v=TvxhuAUJGUg&list=PLGRDMO4rOGcMh2fAMOnwuBMDa8PxiKWoN

Start coding in a [ready-to-code development environment](https://www.gitpod.io):

Or you can run it from Maven directly using the Spring Boot Maven plugin. If you do this it will pick up changes that you make in the project immediately (changes to Java source files require a compile as well - most people use an IDE for this):

```
./mvnw spring-boot:run
```

## Modification de l'image gitpod de base:
https://www.gitpod.io/docs/introduction/languages/java



Update version Java :
```
sdk update
sdk list java
sdk install java 17.0.5-tem
```

Packaging & run de l'application :
```
./mvnw package -DskipTests
java -jar target/*.jar
```


# RabbitMQ
https://www.rabbitmq.com/
https://www.rabbitmq.com/getstarted.html


## Protocoles:
AMQP, STOMP, MQTT, HTTP, ...

## Vocabulaire:

### Message broker
A message broker is a piece of software, which enables services and applications to communicate with each other using messages. The message structure is formally defined and independent from the services that send them.

### Message

### Producer
the application responsible for sending messages. It’s connected with the message broker. In publish/subscribe pattern they are called publishers

### Consumer
the endpoint that consumes messages waiting in the message broker. In publish/subscribe pattern they are called subscribers

### Queue/topic
A folder in a filesystem. Message broker uses them to store messages.

### Exchange
### Routing Key
### Binding

### Two most common distribution patterns
#### Point-to-point messaging
#### Publish/subscribe
...

## Examples of message brokers
RabbitMQ, Apache ActiveMQ, Apache Kafka, Redis, Amazon SQS, and Amazon SNS


## Install & setup using Docker
cf. vidéo : Spring Boot RabbitMQ Tutorial - #5 - Install and Setup RabbitMQ using Docker
Images officielles : https://hub.docker.com/_/rabbitmq

docker pull rabbitmq:3.11.6-management
docker run -d --hostname my-rabbit --name some-rabbit rabbitmq:3.11.6-management
docker run --rm -it --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3.11.6-management
docker run -it --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3.11.6-management

Console d'administration
http://localhost:15672
guest/guest

cf. vidéo : Spring Boot RabbitMQ Tutorial - #6 - Explore RabbitMQ using RabbitMQ Management UI
Création des premiers éléments : Exchange, Queue, Routing key + test d'envoi d'un message sur l'exchange et récupération du message dans la queue demo.

Commencer par créer un nouveau vhost, test par exemple et travailler avec ce vhost

Si besoin de faire des actions en ligne de commande :
Création d'un nouveau user via les outils disponibles dans le container rabbitMQ
Connexion au container
  docker exec -it <container_id> bash

Commande d'administration rabbitMQ
  rabbitmqctl add_user <username> <password>
  rabbitmqctl set_user_tags <username> administrator
  rabbitmqctl set_permissions --vhost / <username> ".*" ".*" ".*"


vidéo : Spring Boot RabbitMQ Tutorial - #7 - Create and Set up Spring Boot 3 Project in IntelliJ IDEA
https://www.youtube.com/watch?v=uXMHAOS1cxE&list=PLGRDMO4rOGcMh2fAMOnwuBMDa8PxiKWoN&index=7

Initialisation du projet Spring Boot 3 à partir de Spring initializr (https://start.spring.io/)
Attention, utilisation de Java 17 avec Spring Boot 3 !!!!
Présentation de la lib Spring AMQP : https://spring.io/projects/spring-amqp


vidéo : Spring Boot RabbitMQ Tutorial - #8 - Connection Between Spring Boot and RabbitMQ
https://www.youtube.com/watch?v=5n5rBnnOKgg&list=PLGRDMO4rOGcMh2fAMOnwuBMDa8PxiKWoN&index=8
@todo : reprendre ici



## Librairies utiles
Spring Boot / Spring AMQP (https://spring.io/projects/spring-amqp)
-> starter : Spring for RabbitMQ (MESSAGING)
+ dans le tuto Java Guides : Spring Web + Lombok => projet ouvert avec IntelliJ

### Configuration projet Spring Boot:
configuration par défaut (Spring Boot Autoconfiguration for Spring AMQP):
Define these properties in a application.properties:
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest










## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yoank78/learning-rabbitmq.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yoank78/learning-rabbitmq/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
